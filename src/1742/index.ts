import { count } from "console";

function countBalls(lowLimit: number, highLimit: number): number {
    // 46 since the limit of the input at a max is 100_000 so for each 0th position max value is 9 and 5 positions so 9*5 is the max length of the array
    let output = new Array(46).fill(0);
    let max = 0;
    for (let i = lowLimit; i <= highLimit; i += 1) {
        let num = i;
        let sum = 0;
        while (num > 0) {
            sum += num % 10;
            num /= 10;
            num = num | 0;
        }
        let index = Math.floor(sum);
        output[index]+=1;
        max = Math.max(output[index],max);
    }
    return max
};
export const main = () => {
    console.log(countBalls(6, 100))
}