function largestAltitude(gain: number[]): number {
    let starting = 0;
    let output = 0;
    for (let i =0; i<gain.length;i+=1){
        starting+=gain[i];
        output = Math.max(starting,output);
    }
    return output;
};

export const main = () => {
    console.log(largestAltitude([-5, 1, 5, 0, -7]))
}