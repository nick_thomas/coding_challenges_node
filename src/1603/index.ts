class ParkingSystem {
    private big: number;
    private medium: number;
    private small: number;

    constructor(big: number, medium: number, small: number) {
        this.big = big;
        this.medium = medium;
        this.small = small;
    }

    addCar(carType: number): boolean {
        switch (carType) {
            case 1: if (this.big > 0) {
                this.big -= 1;
                return true;
            } else {
                return false;
            }
            case 2: if (this.medium > 0) {
                this.medium -= 1;
                return true;
            } else {
                return false;
            }
            case 3: if (this.small > 0) {
                this.small -= 1;
                return true;
            } else {
                return false;
            }
            default: return false;
        }
    }
}
export const main = () => {
    const parking = new ParkingSystem(1, 1, 0);
    console.log(parking.addCar(1))
    console.log(parking.addCar(2))
    console.log(parking.addCar(3))
}