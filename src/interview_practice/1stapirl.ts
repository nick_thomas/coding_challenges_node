// https://leetcode.com/problemset/all/?listId=wpwgkgt&page=1&sorting=W3sic29ydE9yZGVyIjoiQVNDRU5ESU5HIiwib3JkZXJCeSI6IkRJRkZJQ1VMVFkifV0%3D
export function arrayToString<T>(arr: T[]): string {
  return JSON.stringify(arr);
}
function reverseString(s: string[]): void {
  // what happens when the string is empty?
  // length of the string is greater than 10^5
  // are all the strings ascii characters?
  // can we modify the input array?
  // how d owe solve this?
  // iterate through the list and swap till mid point
  // if the length is even or if the length is odd
  const length = Math.floor(s.length / 2);
  const offset = 1;
  for (let i = 0; i < length; i += 1) {
    let currentIndex = i;
    let swapPosition = s.length - i - offset;
    [s[currentIndex], s[swapPosition]] = [s[swapPosition], s[currentIndex]];
  }
}

function checkPalindrome(s: string, first: number, last: number): boolean {
  while (first < last) {
    if (s[first] !== s[last]) {
      return false;
    }
    first += 1;
    last -= 1;
  }
  return true;
}

function validPalindrome(s: string): boolean {
  // input s string -> can it be empty?
  // what is palindrome? first and last element of a string are equal
  // use two pointers to iterate and check
  // at most delete one character -> skip forward/backward by one
  let [first, last] = [0, s.length - 1];
  while (first < last) {
    if (s[first] !== s[last]) {
      // skips forward to check if one skip one element
      return (
        checkPalindrome(s, first, last - 1) ||
        checkPalindrome(s, first + 1, last)
      );
    }
    first += 1;
    last -= 1;
  }
  return true;
}

function nextPermutation(nums: number[]): void {
  // sort the next logical permutation, must be lexigraphically bigger
  // if already lexigraphically the biggest, start from lowest order possible
  // compare the first and second number, if first > second, swap to end
  // https://leetcode.com/problems/next-permutation/discuss/199319/Javascript-with-explanation
}

const practiceApril1 = () => {
  // console.assert(validPalindrome("abca"));
  let arr = [1, 2, 3];
  nextPermutation(arr);
  console.assert(arrayToString(arr) === arrayToString([1, 3, 2]));
};

export default practiceApril1;
