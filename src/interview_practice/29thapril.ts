export function findCenter(edges: number[][]): number {
  const length = edges.length;
  const visited: Array<number> = [];
  for (const edge of edges) {
    for (const item of edge) {
      visited[item] = visited[item] === undefined ? 1 : (visited[item] += 1);
    }
  }
  return visited.findIndex((item) => item === length);
}

// https://leetcode.com/problems/all-paths-from-source-to-target/
export function allPathsSourceTarget(graph: number[][]): number[][] {
  let output: Array<Array<number>> = [];
  const dfs = (i: number, stack: number[]) => {
    if (stack.includes(i)) return;
    stack.push(i);
    if (i === graph.length - 1) output.push(stack);
    else graph[i].forEach((node) => dfs(node, [...stack]));
  };
  dfs(0, []);
  return output;
}

// everyone trusts x
// x trusts no one
export function findJudge(n: number, trust: number[][]): number {
  if (!trust.length && n === 1) return n;
  let maxFrequencyTrust = -Infinity;
  let trustMap: Map<number, number> = new Map();
  for (const [trustee, truster] of trust) {
    if (!trustMap.has(truster)) {
      trustMap.set(truster, 1);
    } else {
      trustMap.set(truster, trustMap.get(truster)! + 1);
    }
    const freq = trustMap.get(truster);
    if (freq !== undefined && freq > maxFrequencyTrust) {
      maxFrequencyTrust = freq;
    }
  }
  const trustMapper = new Map(trust as any);
  for (let i = 1; i <= n; i += 1) {
    if (!trustMapper.has(i) && maxFrequencyTrust === n - 1) return i;
  }
  return -1;
}
// https://leetcode.com/explore/interview/card/top-interview-questions-medium/
export function findSmallestSetOfVertices(
  n: number,
  edges: number[][]
): number[] {
  let map: Map<number, number> = new Map();
  for (let i = 0; i < n; i++) {
    map.set(i, 0);
  }
  for (const [_, out] of edges) {
    map.set(out, map.get(out)! + 1);
  }
  let output = [];
  for (const [key, val] of map) {
    if (val === 0) {
      output.push(key);
    }
  }
  return output;
}

const graph29 = () => {};
export default graph29;
