// https://www.hackerrank.com/challenges/one-week-preparation-kit-plus-minus/problem?isFullScreen=true&h_l=interview&playlist_slugs%5B%5D=preparation-kits&playlist_slugs%5B%5D=one-week-preparation-kit&playlist_slugs%5B%5D=one-week-day-one
function plusMinus(arr: number[]): void {
  // Write your code here
  let countOfArr = new Map();
  countOfArr.set("p", 0);
  countOfArr.set("n", 0);
  countOfArr.set("z", 0);
  for (let item of arr) {
    if (item > 0) {
      countOfArr.set("p", countOfArr.get("p") + 1);
    } else if (item === 0) {
      countOfArr.set("z", countOfArr.get("z") + 1);
    } else {
      countOfArr.set("n", countOfArr.get("n") + 1);
    }
  }
  const total = arr.length;
  for (const [key, item] of countOfArr) {
    let value = (item / total).toFixed(5);
    console.log(value);
  }
}

function miniMaxSum(arr: number[]): void {
  // Write your code here
  arr.sort((a, b) => {
    let firstNumber = Number(a);
    let secondNumber = Number(b);
    if (firstNumber > secondNumber) {
      return 1;
    } else if (firstNumber < secondNumber) {
      return -1;
    } else {
      return 0;
    }
  });
  let maxMin = arr
    .slice(0, arr.length - 1)
    .reduce((prev, cur) => prev + cur, 0);
  let maxMax = arr
    .reverse()
    .slice(0, arr.length - 1)
    .reduce((prev, cur) => prev + cur, 0);
  console.log(`${maxMin}  ${maxMax}`);
}

function timeConversion(s: string): string {
  // Write your code here
  let timeOfDay = s.trim().slice(s.length - 2, s.length);
  let [hour, min, sec] = s.trim().split(":");
  let seconds = sec.replace(timeOfDay, "");
  if (timeOfDay === "PM") {
    if (hour === "12") {
      return `${hour}:${min}:${seconds}`;
    }
    return `${Number(hour) + 12}:${min}:${seconds}`;
  }
  if (hour === "12") {
    return `0${Number(hour) - 12}:${min}:${seconds}`;
  }
  return `${Number(hour)}:${min}:${seconds}`;
}

function findMedian(arr: number[]): number {
  // Write your code here
  arr.sort((a, b) => {
    let firstNumber = Number(a);
    let secondNumber = Number(b);
    if (firstNumber > secondNumber) {
      return 1;
    } else if (firstNumber < secondNumber) {
      return -1;
    } else {
      return 0;
    }
  });
  let median = Math.round(arr.length / 2);
  return arr[median - 1];
}

function checkPalindrome(s: string): boolean {
  let punctuation = `,' `;
  let newString = s
    .split("")
    .filter((item) => punctuation.indexOf(item) === -1)
    .join("");
  if (s.length === 0) {
    return true;
  }
  let [first, last] = [
    newString[0].toLowerCase(),
    newString[newString.length - 1].toLowerCase(),
  ];
  if (first === last) {
    return checkPalindrome(newString.slice(1, newString.length - 1));
  }
  return false;
}

const main24th = () => {
  let stringer = "Murder for a jar of red rum";
  console.log(checkPalindrome(stringer));
};
export default main24th;
