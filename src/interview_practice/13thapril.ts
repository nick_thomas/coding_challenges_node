function isValid(s: string): boolean {
  if (s.length < 2) {
    return false;
  }
  let stack = [];
  for (const item of s.split("")) {
    if (item === "(") {
      stack.push(")");
      continue;
    }
    if (item === "{") {
      stack.push("}");
      continue;
    }
    if (item === "[") {
      stack.push("]");
      continue;
    }
    if (stack.length > 0 && item === stack[stack.length - 1]) {
      stack.pop();
      continue;
    } else {
      return false;
    }
    stack.push(item);
  }

  return stack.length === 0 ? true : false;
}

class ListNode {
  val: number;
  next: ListNode | null;
  constructor(val?: number, next?: ListNode | null) {
    this.val = val === undefined ? 0 : val;
    this.next = next === undefined ? null : next;
  }
}

function mergeTwoLists(
  list1: ListNode | null,
  list2: ListNode | null
): ListNode | null {
  let sortedList = [];
  while (list1 !== null) {
    sortedList.push(list1.val);
    list1 = list1.next;
  }
  while (list2 !== null) {
    sortedList.push(list2.val);
    list2 = list2.next;
  }
  sortedList.sort((a, b) => a - b);
  let output: ListNode | null = null;
  while (sortedList.length !== 0) {
    let item = sortedList.pop();
    if (output === null) {
      output = new ListNode(item);
    } else {
      let newNode = new ListNode(item);
      newNode.next = output;
      output = newNode;
    }
  }
  return output;
}

function mergeKLists(lists: Array<ListNode | null>): ListNode | null {
  let sortedList = [];
  for (let list of lists) {
    while (list !== null) {
      sortedList.push(list.val);
      list = list.next;
    }
  }
  sortedList.sort((a, b) => a - b);
  let output: ListNode | null = null;
  while (sortedList.length !== 0) {
    let item = sortedList.pop();
    if (output === null) {
      output = new ListNode(item);
    } else {
      let newNode = new ListNode(item);
      newNode.next = output;
      output = newNode;
    }
  }
  return output;
}

// https://leetcode.com/problems/search-in-rotated-sorted-array/
function search(nums: number[], target: number): number {
  if (nums.length === 0) {
    return -1;
  }
  let [left, right] = [0, nums.length - 1];
  while (left < right) {
    let midpoint = Math.floor(left + (right - left) / 2);
    if (nums[midpoint] > nums[right]) {
      left = midpoint + 1;
    } else {
      right = midpoint;
    }
  }
  let start = left;
  left = 0;
  right = nums.length - 1;
  if (target >= nums[start] && target <= nums[right]) {
    left = start;
  } else {
    right = start;
  }
  while (left <= right) {
    let midpoint = Math.floor(left + (right - left) / 2);
    if (nums[midpoint] === target) {
      return midpoint;
    } else if (nums[midpoint] < target) {
      left = midpoint + 1;
    } else {
      right = midpoint - 1;
    }
  }
  return -1;
}

function combination(
  candidates: Array<number>,
  target: number,
  combSum: Array<number>,
  curIndex: number,
  curSum: number,
  ans: Array<Array<number>>
): void {
  if (curSum > target) return;
  if (curSum === target) {
    ans.push([...combSum]);
    return;
  }
  for (let i = curIndex; i < candidates.length; i += 1) {
    combSum.push(candidates[i]);
    curSum += candidates[i];
    combination(candidates, target, combSum, i, curSum, ans);
    combSum.pop();
    curSum -= candidates[i];
  }
}

// https://leetcode.com/problems/combination-sum/
// https://leetcode.com/problems/combination-sum/discuss/1777569/FULL-EXPLANATION-WITH-STATE-SPACE-TREE-oror-Recursion-and-Backtracking-oror-Well-Explained-oror-C%2B%2B
function combinationSum(candidates: number[], target: number): number[][] {
  // if the sum of combinations of each is greater than target. return
  // if the sum of combinations is equal to target, add combinations to answer
  // if the cum of combinations is less, continue backtracking
  let ans: Array<Array<number>> = [];
  let combSum: Array<number> = [];
  combination(candidates, target, combSum, 0, 0, ans);
  return ans;
}
const thirteenthApril = () => {
  //  console.log(isValid("()[]{}"));
  //  console.log(isValid("()"));
  //  console.log(isValid("(}"));
  //  console.log(isValid("}"));
  //  console.log(isValid(")(){}"));
  //  console.log(isValid("([}}])"));
  //  console.log(mergeTwoLists(list1, list2));
  //  const list1 = new ListNode(1, new ListNode(2, new ListNode(4, null)));
  //  const list2 = new ListNode(1, new ListNode(3, new ListNode(4, null)));
  //  const list3 = new ListNode(2, new ListNode(4, null));
  //  console.log(mergeKLists([list1, list2, list3]));
  //  console.log(search([4, 5, 6, 7, 0, 1, 2], 3));
  //  console.log(search([4, 5, 6, 7, 0, 1, 2], 0));
  //  console.log(search([1], 0));
  console.log(combinationSum([2, 3, 6, 7], 7));
};

export default thirteenthApril;
