import {
  allPathsSourceTarget,
  findCenter,
  findJudge,
  findSmallestSetOfVertices,
} from "./29thapril";

it("the default case for center of star graph", () => {
  const edges = [
    [1, 2],
    [2, 3],
    [4, 2],
  ];
  expect(findCenter(edges)).toEqual(2);
});
it("the secondary case for center of star graph", () => {
  const edges = [
    [1, 2],
    [5, 1],
    [1, 3],
    [1, 4],
  ];
  expect(findCenter(edges)).toEqual(1);
});

it("all paths from source target, happy path", () => {
  const edges = [[1, 2], [3], [3], []];
  const output = [
    [0, 1, 3],
    [0, 2, 3],
  ];
  expect(allPathsSourceTarget(edges)).toEqual(output);
});
it("all paths from source target, not equal path", () => {
  const edges = [[4, 3, 1], [3, 2, 4], [3], [4], []];
  const output = [
    [0, 4],
    [0, 3, 4],
    [0, 1, 3, 4],
    [0, 1, 2, 3, 4],
    [0, 1, 4],
  ];
  expect(allPathsSourceTarget(edges)).toEqual(output);
});

it("checks the trust for 2 people", () => {
  const n = 2;
  const trust = [[1, 2]];
  expect(findJudge(n, trust)).toEqual(2);
});

it("checks the trust for 3 people", () => {
  const n = 3;
  const trust = [
    [1, 3],
    [2, 3],
  ];
  expect(findJudge(n, trust)).toEqual(3);
});

it("checks the trust for 3 people, but returns -1,not found", () => {
  const n = 3;
  const trust = [
    [1, 3],
    [2, 3],
    [3, 1],
  ];
  expect(findJudge(n, trust)).toEqual(-1);
});

it("checks the trust for 3 people who trust each other, but returns -1,not found", () => {
  const n = 3;
  const trust = [
    [1, 2],
    [2, 3],
  ];
  expect(findJudge(n, trust)).toEqual(-1);
});

it("minimum number of verticies for [0,3]", () => {
  const n = 6;
  const edges = [
    [0, 1],
    [0, 2],
    [2, 5],
    [3, 4],
    [4, 2],
  ];
  const output = [0, 3];
  expect(findSmallestSetOfVertices(n, edges)).toEqual(output);
});
