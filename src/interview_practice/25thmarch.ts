// https://www.geeksforgeeks.org/check-if-a-word-exists-in-a-grid-or-not/
// frequency of characters
// https://www.glassdoor.ca/Interview/-What-s-the-difference-between-CSS-display-types-inline-block-inline-block-flex-What-is-React-Fragment-component-QTN_4695888.htm
// https://www.glassdoor.ca/Interview/Asked-to-write-algorithms-on-string-palindrome-multiply-two-large-String-number-implement-hashmap-QTN_4496010.htm
// Focus on bind/apply in JavaScript
// Implement promisify
// How to code a function returning true if a given String is a valid email address.
// valid parentheses
// https://www.youtube.com/watch?v=THh3EiUbysk
// https://codesandbox.io/s/magical-babbage-srr2ft?file=/src/App.tsx
function minWindow(s: string, t: string): string {
  //  https://github.com/ed-roh/algorithms/blob/master/leetcode-76.js
  let charCount = t.length;
  let minCount = Number.MAX_VALUE;
  let minCountIndex = 0;
  const char = new Map();
  for (const item of t.split("")) {
    char.set(item, (char.get(item) || 0) + 1);
  }
  for (let left = 0, right = 0; right < s.length; ) {
    let currentItem = s[right];
    if (char.get(currentItem) > 0) charCount--;
    char.set(currentItem, char.get(currentItem) - 1);
    right++;
    while (charCount === 0) {
      if (right - left < minCount) {
        minCount = right - left;
        minCountIndex = left;
      }
      let currentItems = s[left];
      char.set(currentItems, char.get(currentItems) + 1);
      if (char.get(currentItems) > 0) charCount++;
      left++;
    }
  }
  return minCount === Number.MAX_VALUE ? "" : s.substr(minCountIndex, minCount);
}

function frequencyCharacters(s: string): Map<string, number> {
  const output: Map<string, number> = new Map();
  for (const item of s.trim().split("")) {
    output.set(item.toLowerCase(), (output.get(item.toLowerCase()) || 0) + 1);
  }
  return output;
}
function reducePlayground() {
  const jokes = [
    {
      category: "Pun",
      type: "twopart",
      setup: "What did the customer say to the waiter?",
      delivery: "I'm all fed up with your service.",
      flags: {
        nsfw: false,
        religious: false,
        political: false,
        racist: false,
        sexist: false,
        explicit: false,
      },
      id: 190,
      safe: true,
      lang: "en",
    },
    {
      category: "Dark",
      type: "twopart",
      setup: "What's the difference between a baby and a watermelon?",
      delivery:
        "One's satisfying to hit with a sledgehammer. The other's a watermelon.",
      flags: {
        nsfw: false,
        religious: false,
        political: false,
        racist: false,
        sexist: false,
        explicit: true,
      },
      id: 121,
      safe: false,
      lang: "en",
    },
  ];
  interface JokesI {
    category: string;
    type: string;
    setup: string;
    delivery: string;
    flags: {
      nsfw: boolean;
      religious: boolean;
      political: boolean;
      racist: boolean;
      sexist: boolean;
      explicit: boolean;
    };
    id: number;
    safe: boolean;
    lang: string;
  }
  const newJokes = jokes.reduce((prev: Array<JokesI>, cur: JokesI) => {
    if (cur.flags.explicit) {
      prev.push(cur);
      return prev;
    }
    return prev;
  }, [] as Array<JokesI>);
  const newJokes2 = jokes.map((item) => {
    let { category, ...rest } = item;
    const newObj = {};
    // newObj[category] = rest;
    return newObj;
  });
  console.log(newJokes2);
}

function promisify(promiseMe: Function): Function {
  return (...args: any): Promise<any> =>
    new Promise((resolve, reject) => {
      const [value, callback, ...rest] = args;
      if (value === "") {
        reject(new Error("Something really went wrong"));
      }
      resolve(promiseMe(value, callback));
    });
}

const march25th = () => {
  //  reducePlayground();
  //  function something(value: string, callback: (input: string) => string) {
  //    callback(value);
  //  }
  //  const promised = promisify(something);
  //  promised("something new", console.log).then(() => console.log("we good son"));
  //  promised("", console.log)
  //    .catch((e: Error) => console.log(e.message))
  //    .finally(() => console.log("completed"));
  const promise1 = Promise.resolve(42);
  const promise2 = Promise.reject("error");
  const promise3 = new Promise((resolve, reject) => {
    setTimeout(resolve, 100, "foo");
  });

  const newValues = Promise.allSettled([promise1, promise2, promise3])
    .then((values) => {
      console.log(values);
    })
    .catch((e) => console.log(e));
};

export default march25th;
