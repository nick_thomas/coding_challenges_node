function maxAreaAlt(height: number[]): number {
  let max = -Infinity;
  let [start, end] = [0, height.length - 1];
  while (start < end) {
    let min = Math.min(height[start], height[end]);
    max = Math.max(max, min * (end - start));
    height[start] < height[end] ? (start += 1) : (end -= 1);
  }
  return max;
}
function maxArea(height: number[]): number {
  let max = -Infinity;
  for (let i = 0; i < height.length; i += 1) {
    for (let j = i; j < height.length; j += 1) {
      let min = Math.min(height[i], height[j]);
      max = Math.max(max, min * (j - i));
    }
  }
  return max;
}
function threeSum(nums: number[]): number[][] {
  nums.sort((a, b) => a - b);
  const result: Array<Array<number>> = [];
  if (nums.length < 3) return result;
  for (let i = 0; i < nums.length; i += 1) {
    let left = i + 1;
    let right = nums.length - 1;
    if (i > 0 && nums[i] === nums[i - 1]) continue;
    while (left < right) {
      let sum = nums[i] + nums[left] + nums[right];
      if (sum === 0) {
        const output = [nums[i], nums[left], nums[right]];
        result.push(output);
        left += 1;
        while (nums[left] === nums[left - 1] && left < right) left += 1;
      } else if (sum > 0) {
        right -= 1;
      } else {
        left += 1;
      }
    }
  }
  return result;
}

class ListNode {
  val: number;
  next: ListNode | null;
  constructor(val?: number, next?: ListNode | null) {
    this.val = val === undefined ? 0 : val;
    this.next = next === undefined ? null : next;
  }
}

function addTwoNumbers(
  l1: ListNode | null,
  l2: ListNode | null
): ListNode | null {
  let carry = 0;
  let resultNode: ListNode = new ListNode(0);
  let returnNode = resultNode;
  while (l1 || l2) {
    const sum = (l1 ? l1.val : 0) + (l2 ? l2.val : 0) + carry;
    resultNode.next = new ListNode(sum >= 10 ? sum % 10 : sum);
    carry = sum >= 10 ? Math.floor(sum / 10) : 0;
    l1 = l1 ? l1.next : null;
    l2 = l2 ? l2.next : null;
    resultNode = resultNode.next;
  }
  if (carry > 0) {
    resultNode.next = new ListNode(carry);
  }
  return returnNode.next;
}

const pigIt = (a: string): string => {
  // code away
  let strings = a.split(" ");
  let output = [];
  for (const item of strings) {
    const charCode = item.toLowerCase().charCodeAt(0);
    if (charCode >= 97 && charCode <= 122) {
      let [first, ...rest] = item.split("");
      output.push(`${[...rest, ...first].join("")}ay`);
    } else {
      output.push(`${item}`);
    }
  }
  return output.join(" ");
};
function removeNthFromEnd(head: ListNode | null, n: number): ListNode | null {
  let node = head;
  let numbers = [];
  while (node !== null) {
    numbers.push(node.val);
    node = node.next;
  }
  if (numbers.length === 1 && n === 1) {
    return null;
  }
  const removeNthFromEnd = numbers.length - n;
  numbers.splice(removeNthFromEnd, 1);
  numbers.reverse();
  let newNode: ListNode | null = null;
  while (numbers.length > 0) {
    let item = numbers.shift();
    if (newNode === null) {
      newNode = new ListNode(item);
    } else {
      const newItem = new ListNode(item);
      newItem.next = newNode;
      newNode = newItem;
    }
  }
  return newNode;
}
const eleventhApril = () => {
  //  console.log(maxArea([1, 8, 6, 2, 5, 4, 8, 3, 7]));
  //  console.log(maxAreaAlt([1, 8, 6, 2, 5, 4, 8, 3, 7]));
  // console.log(threeSum([-1, 0, 1, 2, -1, 4]));
  // const l1 = new ListNode(2, new ListNode(4, new ListNode(3)));
  // const l2 = new ListNode(5, new ListNode(6, new ListNode(4)));
  // console.log(addTwoNumbers(l1, l2));
  //  console.log(pigIt("Pig latin is cool"));
  //  console.log(pigIt("Hello world !"));
  let node = new ListNode(
    1,
    new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5, null))))
  );
  console.log(removeNthFromEnd(node, 2));
};

export default eleventhApril;
