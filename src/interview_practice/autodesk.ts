const autodesk = () => {
  let arr = ["apples", "pineapples", "oranges", "apples", "oranges"];
  let count = 0;
  const arrofApples = arr.forEach((item) => {
    if (item === "apples") {
      count++;
    }
  });
  const countOfFruits = new Map();
  const something = "";
  for (const item of arr) {
    if (countOfFruits.get(item) === undefined) {
      countOfFruits.set(item, 0);
    }
    countOfFruits.set(item, countOfFruits.get(item) + 1);
  }
  // Ex: arr = ['apples', 'apples', 'oranges', 'apples', 'pineapples', 'pineapples', 'oranges'];
  // "apples2oranges1apples1pineapples2oranges1"
  // "apples2oranges1apples1pineapples2oranges1"
  arr = [
    "apples",
    "apples",
    "oranges",
    "apples",
    "pineapples",
    "pineapples",
    "oranges",
  ];
  let output = "";
  let stack = [];
  for (const [index, item] of Object.entries(arr)) {
    if (stack.length === 0) {
      stack.push(item);
      continue;
    }
    if (stack[stack.length - 1] === item) {
      stack.push(item);
      continue;
    }
    if (stack[stack.length - 1] !== item) {
      let count = stack.length;
      let items = stack.pop();
      output += `${items}${count}`;
      stack = [];
    }
    if (+index === arr.length - 1) {
      output += `${item}1`;
      break;
    }
    stack.push(item);
  }
  console.log(output);
};
export default autodesk;
