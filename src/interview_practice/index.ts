import eleventhApril from "./11thapril.js";
import thirteenthApril from "./13thapril.js";
import ninetheenthApril from "./19thapril.js";
import practiceApril1 from "./1stapirl.js";
import twentyFourthApril from "./24thapril.js";
import main24th from "./24th_march.js";
import march25th from "./25thmarch.js";
import graph29 from "./29thapril.js";
import fourthApril from "./4thapril.js";
import sixthApril from "./6thapril.js";
import eigthApril from "./8thapril.js";
import autodesk from "./autodesk.js";
import klarnamain from "./klarnaassesment.js";

const main = () => {
  graph29();
};

export default main;
