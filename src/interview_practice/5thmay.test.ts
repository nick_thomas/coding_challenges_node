import {
  EncoderDecoder,
  lengthOfLongestSubstring,
  setZeroes,
  threeSum,
} from "./5thmay";

it("simple encoder decoder", () => {
  const input = ["lint", "code", "love", "you"];
  const output = ["lint", "code", "love", "you"];
  const delim = "!:!";
  const encoderDecoder = new EncoderDecoder(input, delim);
  expect(encoderDecoder.decode(encoderDecoder.encode())).toEqual(output);
});

it("3sum works for simple input", () => {
  const input = [-1, 0, 1, 2, -1, 4];
  const output = [
    [-1, -1, 2],
    [-1, 0, 1],
  ];
  expect(threeSum(input)).toEqual(output);
});

it("replaces adjacent 0s of row and column", () => {
  const input = [
    [1, 1, 1],
    [1, 0, 1],
    [1, 1, 1],
  ];
  const output = [
    [1, 0, 1],
    [0, 0, 0],
    [1, 0, 1],
  ];
  setZeroes(input);
  expect(input).toEqual(output);
});
it("replaces adjacent 0s of row and column with multiple zeroes", () => {
  const input = [
    [0, 1, 2, 0],
    [3, 4, 5, 2],
    [1, 3, 1, 5],
  ];
  const output = [
    [0, 0, 0, 0],
    [0, 4, 5, 0],
    [0, 3, 1, 0],
  ];
  setZeroes(input);
  expect(input).toEqual(output);
});

it("longest substring of abcabcbb", () => {
  let input = "abcabcbb";
  let output = 3;
  expect(lengthOfLongestSubstring(input)).toEqual(output);
});
