class ListNode {
  val: number;
  next: ListNode | null;
  constructor(val?: number, next?: ListNode | null) {
    this.val = val === undefined ? 0 : val;
    this.next = next === undefined ? null : next;
  }
}
function hasCycle(head: ListNode | null): boolean {
  let set = new Set();
  while (head !== null) {
    let curr = head;
    if (set.has(curr)) {
      return true;
    }
    set.add(curr);
    head = head.next;
  }
  return false;
}

function detectCycle(head: ListNode | null): ListNode | null {
  let [slow, fast] = [head, head];
  while (fast !== null && fast.next !== null) {
    if (slow !== null) {
      slow = slow.next;
    }
    fast = fast.next.next;
    if (slow === fast) {
      slow = head;
      while (slow !== fast) {
        if (slow !== null && fast !== null) {
          slow = slow.next;
          fast = fast.next;
        }
      }
      return slow;
    }
  }
  return null;
}

function deleteDuplicates(head: ListNode | null): ListNode | null {
  let curr_node = head;
  while (curr_node !== null && curr_node.next !== null) {
    if (curr_node.next.val === curr_node.val) {
      curr_node.next = curr_node.next.next;
    } else {
      curr_node = curr_node.next;
    }
  }
  return head;
}

function twoSum(nums: number[], target: number): number[] {
  let output: Array<number> = [];
  let map: Map<number, number> = new Map();
  for (let [index, item] of nums.entries()) {
    let offset = target - item;
    if (!map.has(item)) {
      map.set(offset, index);
    } else {
      const mapValue = map.get(item);
      if (mapValue !== undefined) {
        output.push(mapValue);
        output.push(index);
        break;
      }
    }
  }
  return output;
}

function groupAnagrams(strs: string[]): string[][] {
  const map: Map<string, string[]> = new Map();
  for (const item of strs) {
    const offset = item
      .split("")
      .sort((a, b) => a.localeCompare(b))
      .join("");
    if (map.has(offset)) {
      let output = map.get(offset)!;
      output.push(item);
      map.set(offset, output);
    } else {
      let output = [];
      output.push(item);
      map.set(offset, output);
    }
  }
  return [...map.values()];
}

const fourthApril = () => {
  // console.log(twoSum([0, 3, -3, 4, -1], -1));
  console.log(
    groupAnagrams([
      "cab",
      "tin",
      "pew",
      "duh",
      "may",
      "ill",
      "buy",
      "bar",
      "max",
      "doc",
    ])
  );
};

export default fourthApril;
