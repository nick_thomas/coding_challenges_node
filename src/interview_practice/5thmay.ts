// https://leetcode.com/explore/interview/card/top-interview-questions-medium/103/array-and-strings/776/
export function threeSum(nums: number[]): number[][] {
  const output: number[][] = [];
  if (nums.length < 3) {
    return output;
  }
  let map: Map<string, number> = new Map();
  nums.sort((a, b) => a - b);
  for (let i = 0; i < nums.length; i += 1) {
    for (let j = i + 1; j < nums.length; j += 1) {
      for (let k = j + 1; k < nums.length; k += 1) {
        if (
          nums[i] + nums[j] + nums[k] === 0 &&
          !map.has(`${nums[i]}${nums[j]}${nums[k]}`)
        ) {
          output.push([nums[i], nums[j], nums[k]]);
          map.set(`${nums[i]}${nums[j]}${nums[k]}`, 0);
        }
      }
    }
  }
  return output;
}

export class EncoderDecoder {
  public inputStr: string[];
  public delim: string;
  constructor(inputStr: string[], delim: string) {
    this.inputStr = [...inputStr];
    this.delim = delim;
  }

  public encode(): string {
    let output = this.inputStr.join(this.delim);
    return output;
  }

  public decode(input: string): string[] {
    let output = input.split(this.delim);
    return output;
  }
}

export class CircularBuffer {}

export function setZeroes(matrix: number[][]): void {
  if (matrix.length === 0) return;
  let row = matrix.length;
  let col = matrix[0].length;
  let zerothRow = new Set();
  let zerothCol = new Set();
  for (let i = 0; i < row; i += 1) {
    for (let j = 0; j < col; j += 1) {
      if (matrix[i][j] === 0) {
        zerothRow.add(i);
        zerothCol.add(j);
      }
    }
  }
  for (let i = 0; i < row; i += 1) {
    for (let j = 0; j < col; j += 1) {
      if (zerothRow.has(i) || zerothCol.has(j)) {
        matrix[i][j] = 0;
      }
    }
  }
}

export function lengthOfLongestSubstring(s: string): number {
  let res = 0;
  let length = s.length;
  for (let i = 0; i < length; i += 1) {
    let visitedMap: Map<number, boolean> = new Map();
    for (let j = i; j < length; j += 1) {
      if (visitedMap.get(s.charCodeAt(j))) {
        break;
      }
      res = Math.max(res, j - i + 1);
      visitedMap.set(s.charCodeAt(j), true);
    }
  }
  return res;
}
