export class TreeNode {
  val: number;
  left: TreeNode | null;
  right: TreeNode | null;
  constructor(val?: number, left?: TreeNode | null, right?: TreeNode | null) {
    this.val = val === undefined ? 0 : val;
    this.left = left === undefined ? null : left;
    this.right = right === undefined ? null : right;
  }
}

// https://leetcode.com/problems/sum-of-nodes-with-even-valued-grandparent/

// if node is even, add all its grand kids to array

export function sumEvenGrandparent(root: TreeNode | null): number {
  if (root === null) {
    return 0;
  }
  let sum = 0;
  if (root.val % 2 === 0) {
    if (root.left !== null && root.left.left !== null) {
      sum += root.left.left.val;
    }
    if (root.left !== null && root.left.right !== null) {
      sum += root.left.right.val;
    }
    if (root.right !== null && root.right.left !== null) {
      sum += root.right.left.val;
    }
    if (root.right !== null && root.right.right !== null) {
      sum += root.right.right.val;
    }
  }
  if (root.left !== null) {
    sum += sumEvenGrandparent(root.left);
  }
  if (root.right !== null) {
    sum += sumEvenGrandparent(root.right);
  }
  return sum;
}

// https://charlieinden.github.io/Interview-Experience/2020-05-20_2--Count-Visible-Nodes-in-Binary-Tree-Microsoft--8a06d627595e.html
// https://www.youtube.com/watch?v=fAAZixBzIAI
export function visibleNode(root: TreeNode | null): number {
  let counter = 0;
  function traverseDepth(root: TreeNode | null, max: number) {
    if (root === null) return;
    if (root.val >= max) counter += 1;
    traverseDepth(root.left, Math.max(root.val, max));
    traverseDepth(root.right, Math.max(root.val, max));
  }
  traverseDepth(root, root !== null ? root.val : 0);
  return counter;
}

const twentyFourthApril = () => {
  const left = new TreeNode(
    7,
    new TreeNode(2, new TreeNode(9), null),
    new TreeNode(7, new TreeNode(1), new TreeNode(4))
  );
  const right = new TreeNode(
    8,
    new TreeNode(1),
    new TreeNode(3, null, new TreeNode(5))
  );
  const root = new TreeNode(6, left, right);
  console.log(sumEvenGrandparent(root)); // 18
};
export default twentyFourthApril;
