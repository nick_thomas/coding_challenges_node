// Trees & Graphs https://scontent.fyzd1-3.fna.fbcdn.net/v/t39.2365-6/75102466_573561830053024_996141617616257024_n.pdf?_nc_cat=105&ccb=1-5&_nc_sid=ad8a9d&_nc_ohc=Gb6Bw3qegZYAX-M5Zhk&_nc_ht=scontent.fyzd1-3.fna&oh=00_AT9jD5pn9o9Aqy2P-TLVjF9UPqEGGdRJ4J1-V81SIgEo9w&oe=6264B618
// Each circles is a node, each line is an edge
// last node of trees are called leaves
// trversal -> navigating trees
// pre-order (DFS)
// in-order
// post-order
// level-order(BFS)
//

class TreeNode {
  val: number;
  left: TreeNode | null;
  right: TreeNode | null;
  constructor(val?: number, left?: TreeNode | null, right?: TreeNode | null) {
    this.val = val === undefined ? 0 : val;
    this.left = left === undefined ? null : left;
    this.right = right === undefined ? null : right;
  }
}

function helper(root: TreeNode | null, result: number[]): void {
  if (root === null) {
    return;
  }
  helper(root.left, result);
  result.push(root.val);
  helper(root.right, result);
}

function inorderTraversal(root: TreeNode | null): number[] {
  let res = [] as number[];
  helper(root, res);
  return res;
}
function checkTree(root: TreeNode | null): boolean {
  if (!root) return false;
  if (
    root.left !== null &&
    root.right !== null &&
    root.left.val !== null &&
    root.right.val !== null
  ) {
    return root.val === root.left.val + root.right.val ? true : false;
  }
  return false;
}

function rangeSumBSThelper(
  root: TreeNode | null,
  low: number,
  high: number,
  res: Array<number>
): void {
  if (root === null) {
    return;
  }
  rangeSumBSThelper(root.left, low, high, res);
  if (root.val >= low && root.val <= high) {
    res.push(root.val);
  }

  rangeSumBSThelper(root.right, low, high, res);
}

function rangeSumBST(root: TreeNode | null, low: number, high: number): number {
  let res: Array<number> = [];
  rangeSumBSThelper(root, low, high, res);
  return res.reduce((prev, cur) => prev + cur);
}
// https://leetcode.com/problems/find-a-corresponding-node-of-a-binary-tree-in-a-clone-of-that-tree/
function getTargetCopy(
  original: TreeNode | null,
  cloned: TreeNode | null,
  target: TreeNode | null
): TreeNode | null {
  if (!cloned) return null;
  if (target !== null && cloned.val === target.val) {
    return cloned;
  } else {
    return (
      getTargetCopy(original, cloned.left, target) ||
      getTargetCopy(original, cloned.right, target)
    );
  }
}
const ninetheenthApril = () => {
  const target = new TreeNode(
    4,
    null,
    new TreeNode(3, null, new TreeNode(2, null, new TreeNode(1, null, null)))
  );
  const root = new TreeNode(
    8,
    null,
    new TreeNode(6, null, new TreeNode(5, null, target))
  );
  const copy = root;
  //console.log(inorderTraversal(root));
  // console.log(checkTree(root));
  //  console.log(rangeSumBST(root, 7, 15));
  console.log(getTargetCopy(root, copy, target));
};

export default ninetheenthApril;
