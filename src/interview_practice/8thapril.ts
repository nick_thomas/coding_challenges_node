function subarraySum(nums: number[], k: number): number {
  let map: Map<number, number> = new Map();
  map.set(0, 1);
  let [output, sum] = [0, 0];
  for (const item of nums) {
    sum += item;
    if (map.has(sum - k)) {
      output += map.get(sum - k)!;
    }
    map.set(sum, map.get(sum)! + 1 || 1);
  }
  return output;
}
function twoSum(nums: number[], target: number): number[] {
  let output: Array<number> = [];
  for (const [index, item] of nums.entries()) {
    let diff = target - item;
    let secondIndex = nums.indexOf(diff, index + 1);
    if (secondIndex > -1) {
      output = [index, secondIndex];
      break;
    }
  }
  return output;
}

function lengthOfLongestSubstring(s: string): number {
  const length = s.length;
  let res = 0;
  for (let i = 0; i < length; i += 1) {
    let visitedMap: Map<number, boolean> = new Map();
    for (let j = i; j < length; j++) {
      if (visitedMap.get(s.charCodeAt(j))) {
        break;
      }
      res = Math.max(res, j - i + 1);
      visitedMap.set(s.charCodeAt(j), true);
    }
  }
  return res;
}

function helper(left: number, right: number, s: string): string {
  let curr = "";
  while (left >= 0 && right < s.length && s[left] === s[right]) {
    curr = s.substring(left, right + 1);
    left -= 1;
    right += 1;
  }
  return curr;
}

function longestPalindrome(s: string): string {
  if (s.length < 2) {
    return s;
  }
  let max = "";
  for (let i = 0; i < s.length; i++) {
    let left = helper(i, i, s);
    let right = helper(i, i + 1, s);
    let currMax = left.length >= right.length ? left : right;
    max = currMax.length > max.length ? currMax : max;
  }
  return max;
}
const eigthApril = () => {
  //console.log(subarraySum([1, 1, 1], 2));
  //  console.log(twoSum([3, 2, 4], 6));
  //  console.log(twoSum([3, 3], 6));
  //  console.log(twoSum([-1, -2, -3, -4, -5], -8));
  // console.log(lengthOfLongestSubstring("abaabcaaaa"));
  console.log(longestPalindrome("aacabdkacaa"));
};

export default eigthApril;
