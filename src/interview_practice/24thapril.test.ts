import { sumEvenGrandparent, visibleNode, TreeNode } from "./24thapril";
test("the sum should be 18", () => {
  const left = new TreeNode(
    7,
    new TreeNode(2, new TreeNode(9), null),
    new TreeNode(7, new TreeNode(1), new TreeNode(4))
  );
  const right = new TreeNode(
    8,
    new TreeNode(1),
    new TreeNode(3, null, new TreeNode(5))
  );
  const root = new TreeNode(6, left, right);
  expect(sumEvenGrandparent(root)).toBe(18);
});

test("the sum should be 0 for single root", () => {
  const root = new TreeNode(1, null, null);
  expect(sumEvenGrandparent(root)).toBe(0);
});

test("the count of visible node is 4", () => {
  const root = new TreeNode(
    5,
    new TreeNode(3, new TreeNode(20, null, null), new TreeNode(21, null, null)),
    new TreeNode(10, new TreeNode(1, null, null), null)
  );
  expect(visibleNode(root)).toEqual(4);
});

test("the count of visible node is 1", () => {
  const root = new TreeNode(
    -1,
    new TreeNode(-2, null, new TreeNode(-3, null, null))
  );
  expect(visibleNode(root)).toEqual(1);
});
test("the count of visible node is 2", () => {
  const root = new TreeNode(
    -1,
    new TreeNode(-2, new TreeNode(1, null, null), new TreeNode(-3, null, null))
  );
  expect(visibleNode(root)).toEqual(2);
});
