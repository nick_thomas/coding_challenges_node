function intersection(nums1: number[], nums2: number[]): number[] {
  let map: Map<number, number> = new Map();
  let output: number[] = [];
  for (const item of nums1) {
    map.set(item, map.get(item)! + 1 || 0);
  }
  for (const item of nums2) {
    if (map.has(item) && output.findIndex((items) => items === item) === -1) {
      output.push(item);
    }
  }

  return output;
}

function removeFilter(input: string): string {
  let [response, _] = input.split("+");
  if (response.includes(".")) {
    return response
      .split("")
      .filter((item) => item !== ".")
      .join("");
  }
  return response;
}

function validDomainName(input: string): boolean {
  return input.includes("+");
}

function numUniqueEmails(emails: string[]): number {
  let map: Set<string> = new Set();
  for (const email of emails) {
    let [localName, domainName] = email.split("@");
    let key = removeFilter(localName) + "@" + domainName;
    map.add(key);
  }
  return map.size;
}

function firstUniqChar(s: string): number {
  let uniqueMap: Map<string, number> = new Map();
  for (const item of s.split("")) {
    uniqueMap.set(item, uniqueMap.get(item)! + 1 || 0);
  }
  const removeDuplicates = [...uniqueMap.entries()].filter(
    ([_item, count]) => count === 0
  );
  let [firstItem, ...rest] = removeDuplicates;
  if (firstItem === undefined) {
    return -1;
  }
  let [first, ...restItems] = firstItem;
  return s.split("").findIndex((item) => item === first);
}
const sixthApril = () => {
  //console.log(intersection([4, 9, 5], [9, 4, 9, 8, 4]));
  /*  console.log(
    numUniqueEmails([
      "test.email+alex@leetcode.com",
      "test.e.mail+bob.cathy@leetcode.com",
      "testemail+david@lee.tcode.com",
    ])
  );*/
  //  console.log(firstUniqChar("leetcode"));
  //  console.log(firstUniqChar("loveleetcode"));
  //  console.log(firstUniqChar("aabb"));
};

export default sixthApril;
