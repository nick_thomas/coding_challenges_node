interface Transaction {
  // always be present and valid
  id: string;
  sourceAccount: string;
  targetAccount: string;
  amount: number | string;
  currency: string;
  category: string;
  time: string;
}

interface UTransaction {
  id: string;
  sourceAccount: string;
  targetAccount?: string;
  category?: string;
  amount: number;
  time: string;
}

const resetTimeToMidnight = (time: any) =>
  new Date(new Date(time).setHours(20, 0, 0, 0));

const validateTransactionArgs = (
  categories: String[],
  start: Date,
  end: Date
) => {
  if (!Array.isArray(categories) || categories.length === 0) {
    throw new Error("Categories is not provided");
  }
  if (
    start === null ||
    start === undefined ||
    !(start instanceof Date) ||
    start.toString() === "Invalid Date"
  ) {
    throw new Error("Start date is missing or invalid");
  }
  if (
    end === null ||
    end === undefined ||
    !(end instanceof Date) ||
    end.toString() === "Invalid Date"
  ) {
    throw new Error("End date is missing or invalid");
  }

  if (resetTimeToMidnight(end) < resetTimeToMidnight(start)) {
    throw new Error("End date must be larger than start date");
  }
};

const isOnSameDateasStart = (
  start: Date,
  end: Date,
  currTransactionTime: Date
) => {
  const midnightStart = resetTimeToMidnight(start).toString();
  const midnightEnd = resetTimeToMidnight(end).toString();
  const midnightTransactionTime =
    resetTimeToMidnight(currTransactionTime).toString();
  return (
    midnightStart === midnightTransactionTime &&
    midnightEnd === midnightTransactionTime
  );
};

const helperValidTimeRange =
  (start: Date, end: Date) => (transaction: Transaction) => {
    const { time } = transaction;
    const currTransactionTime = resetTimeToMidnight(
      new Date(time.substring(0, 10))
    );

    return isOnSameDateasStart(start, end, currTransactionTime)
      ? true
      : currTransactionTime >= start && currTransactionTime < end;
  };

const helperValidCategory =
  (categories: String[]) => (transaction: Transaction) => {
    const { category } = transaction;
    return categories.includes(category);
  };

const helperBalanceGenerator = (balance: any, transaction: any) => {
  const { category, amount } = transaction;
  const parsedToNumber = parseFloat(amount);
  if (balance.hasOwnProperty(category)) {
    balance[category] += parsedToNumber;
    return balance;
  }
  balance[category] = parsedToNumber;
  return balance;
};

const generateEmptyBalance = (categories: String[]) =>
  categories.reduce((totalBalance: any, category: any) => {
    totalBalance[category] = 0;
    return totalBalance;
  }, {});

const generateBalance = (transactions: Transaction[], categories: String[]) => {
  if (transactions.length === 0) {
    return generateEmptyBalance(categories);
  }
  return transactions.reduce(helperBalanceGenerator, Object.create({}));
};

const getBalanceByCategoryInPeriod = (
  transactions: Transaction[],
  categories: String[],
  start: Date,
  end: Date
) => {
  // add your code here
  try {
    validateTransactionArgs(categories, start, end);
    const filterValidTimeRange = helperValidTimeRange(start, end);
    const filterValidCategories = helperValidCategory(categories);
    const filteredTransactions = transactions.filter(
      (transaction) =>
        filterValidCategories(transaction) && filterValidTimeRange(transaction)
    );
    return generateBalance(filteredTransactions, categories);
  } catch (error) {
    return (error as Error).message;
  }
};

const absoluteDifference = (firstAmount: any, secondAmount: any) =>
  Math.abs(
    Math.abs(parseFloat(firstAmount)) - Math.abs(parseFloat(secondAmount))
  );

const validateTransaction = (curAmount: any, amount: any, category: any) =>
  category === undefined || Math.abs(amount) > Math.abs(curAmount);

const mapStoreViaCategory = (item: any, map: any) => {
  if (map.has(item.targetAccount)) {
    let { amount, category } = item;
    let currentAmount = map.get(item.targetAccount)!.amount;
    if (validateTransaction(currentAmount, amount, category)) return;
    let listofCategories = map.get(item.targetAccount);

    map.set(item.targetAccount, [...listofCategories, { amount, category }]);
  } else {
    if (item.category !== undefined) {
      let { amount, category } = item;
      let list = [{ amount, category }];
      map.set(item.targetAccount, list);
    }
  }
};

const appendCategoryForTransaction = (item: any, map: any) => {
  if (!item.hasOwnProperty("category") && item.targetAccount !== undefined) {
    const accounts = map.get(item.targetAccount);
    if (accounts !== null && accounts !== undefined) {
      let minimumDiff = Infinity;
      for (const account of accounts) {
        let { amount, category } = account;
        if (
          absoluteDifference(item.amount, amount) < 1000 &&
          Math.abs(amount) < minimumDiff
        ) {
          item = { ...item, category };
          minimumDiff = Math.abs(amount);
        }
      }
    }
  }
  return item;
};

const categorizeSimilarTransactions = (transactions: any[]) => {
  // add your code here
  if (transactions.length === 0) return [];
  const store = new Map();
  transactions.forEach((item) => mapStoreViaCategory(item, store));
  if (store.size === 0) {
    return transactions;
  }
  console.log(store);
  const updatedTransactions = transactions.map((item) =>
    appendCategoryForTransaction(item, store)
  );
  return updatedTransactions;
};
const klarnamain = () => {
  {
    let transactions = [
      {
        id: "837127ab-f523-4b11-bed3-ae488be4545d",
        sourceAccount: "my_account",
        targetAccount: "fitness_club",
        amount: -11200,
        currency: "EUR",
        category: "sports",
        time: "2021-05-05T01:55:16.646Z",
      },
    ];
    let categories = ["shopping"];
    // invalid date
    const start = new Date("2021-04-01");
    const end = new Date("2021-04-30");
    const output = getBalanceByCategoryInPeriod(
      transactions,
      categories,
      start,
      end
    );
    console.log(output);
  }
  {
    let unrefinedTransactions = [
      {
        id: "a001bb66-6f4c-48bf-8ae0-f73453aa8dd5",
        sourceAccount: "my_account",
        targetAccount: "coffee_shop",
        amount: 350,
        time: "2021-04-10T10:30:00Z",
      },
      {
        id: "bfd6a11a-2099-4b69-a7bb-572d8436cf73",
        sourceAccount: "my_account",
        targetAccount: "coffee_shop",
        amount: -150,
        category: "eating_out",
        time: "2021-03-12T12:34:00Z",
      },
      {
        id: "6359091e-1187-471f-a2aa-81bd2647210f",
        sourceAccount: "my_account",
        targetAccount: "coffee_shop",
        amount: 100,
        category: "entertainment",
        time: "2021-01-12T08:23:00Z",
      },
      {
        id: "a8170ced-1c5f-432c-bb7d-867589a9d4b8",
        sourceAccount: "my_account",
        category: "james",
        targetAccount: "something",
        amount: -190,
        time: "2021-04-12T08:20:00Z",
      },
      {
        id: "a8170ced-1c5f-432c-bb7d-867589a9d4b8",
        sourceAccount: "my_account",
        category: "bond",
        targetAccount: "something",
        amount: -290,
        time: "2021-04-12T08:20:00Z",
      },
      {
        id: "a8170ced-1c5f-432c-bb7d-867589a9d4b8",
        sourceAccount: "my_account",
        category: "bond",
        targetAccount: "something",
        amount: -290,
        time: "2021-04-12T08:20:00Z",
      },
      {
        id: "a8170ced-1c5f-432c-bb7d-867589a9d4b8",
        sourceAccount: "my_account",
        targetAccount: "something",
        amount: -80,
        time: "2021-04-12T08:20:00Z",
      },
    ];
    const output = categorizeSimilarTransactions(unrefinedTransactions);
    console.log(output);
  }
};
export default klarnamain;

//  it('categorization does not overwrite existing category', () => {
//    expect(
//      categorizeSimilarTransactions([
//        {
//          id: 'a001bb66-6f4c-48bf-8ae0-f73453aa8dd5',
//          sourceAccount: 'my_account',
//          targetAccount: 'coffee_shop',
//          amount: -620,
//          time: '2021-04-10T10:30:00Z',
//        },
//        {
//          id: 'bfd6a11a-2099-4b69-a7bb-572d8436cf73',
//          sourceAccount: 'my_account',
//          targetAccount: 'coffee_shop',
//          amount: -350,
//          category: 'eating_out',
//          time: '2021-03-12T12:34:00Z',
//        },
//        {
//          id: 'a8170ced-1c5f-432c-bb7d-867589a9d4b8',
//          sourceAccount: 'my_account',
//          targetAccount: 'coffee_shop',
//          amount: -1690,
//          time: '2021-04-12T08:20:00Z',
//        },
//        {
//          id: 'a8170ced-1c5f-432c-bb7d-867589a9d4be',
//          sourceAccount: 'my_account',
//          targetAccount: 'coffee_shop',
//          category: 'partying',
//          amount: 349,
//          time: '2021-04-12T08:20:00Z',
//        },
//      ])
//    ).toEqual([
//      {
//        id: 'a001bb66-6f4c-48bf-8ae0-f73453aa8dd5',
//        sourceAccount: 'my_account',
//        targetAccount: 'coffee_shop',
//        amount: -620,
//        category: 'partying',
//        time: '2021-04-10T10:30:00Z',
//      },
//      {
//        id: 'bfd6a11a-2099-4b69-a7bb-572d8436cf73',
//        sourceAccount: 'my_account',
//        targetAccount: 'coffee_shop',
//        amount: -350,
//        category: 'eating_out',
//        time: '2021-03-12T12:34:00Z',
//      },
//      {
//        id: 'a8170ced-1c5f-432c-bb7d-867589a9d4b8',
//        sourceAccount: 'my_account',
//        targetAccount: 'coffee_shop',
//        amount: -1690,
//        time: '2021-04-12T08:20:00Z',
//      },
//        {
//          id: 'a8170ced-1c5f-432c-bb7d-867589a9d4be',
//          sourceAccount: 'my_account',
//          targetAccount: 'coffee_shop',
//          category: 'partying',
//          amount: 349,
//          time: '2021-04-12T08:20:00Z',
//        },
//    ]);
//  });
//Error: expect(received).toEqual(expected) // deep equality

//- Expected  - 20
//+ Received  +  0
// Object {
//          "amount": -4400,
//    -     "category": "sports",
//          "currency": "EUR",
//          "id": "000ca786-a300-454d-988b-5be2afbf7f15",
//          "sourceAccount": "my_account",
//          "targetAccount": "bowling_alley",
//          "time": "2021-02-12T22:27:58.515Z",
//        },
//        Object {
//          "amount": -3600,
//    -     "category": "groceries",
//          "currency": "EUR",
//          "id": "1ef987ea-8622-47e2-b90c-aa3495a0e253",
//          "sourceAccount": "my_account",
//          "targetAccount": "supermarket",
//          "time": "2021-02-16T11:18:59.331Z",
//        },
//        Object {
//          "amount": -8300,
//    -     "category": "eating_out",
//          "currency": "EUR",
//          "id": "afe48517-fe02-4376-bd29-a9f586f70220",
//          "sourceAccount": "my_account",
//          "targetAccount": "restaurant",
//          "time": "2021-02-16T01:45:34.798Z",
//        },
//        Object {
//          "amount": -2700,
//    -     "category": "entertainment",
//          "currency": "EUR",
//          "id": "04ed2a63-8e8e-49fd-b341-9dc465d20733",
//          "sourceAccount": "my_account",
//          "targetAccount": "cinema",
//          "time": "2021-02-17T13:13:23.646Z",
//    @@ -529,11 +525,10 @@
//          "targetAccount": "supermarket",
//          "time": "2021-04-02T14:21:13.404Z",
//        },
//        Object {
//          "amount": -3800,
//    -     "category": "sports",
//          "currency": "EUR",
//          "id": "1fbae76e-ec8f-47e5-964d-74192a9ce2a5",
//          "sourceAccount": "my_account",
//          "targetAccount": "bowling_alley",
//          "time": "2021-02-02T18:39:01.765Z",
//    @@ -546,11 +541,10 @@
//          "targetAccount": "fitness_club",
//          "time": "2021-03-16T19:33:18.158Z",
//        },
//        Object {
//          "amount": -7400,
//    -     "category": "entertainment",
//          "currency": "EUR",
//          "id": "0d66438c-5a18-4fc4-bba6-ca7afd95a27d",
//          "sourceAccount": "my_account",
//          "targetAccount": "cinema",
//          "time": "2021-02-09T13:26:17.865Z",
//    @@ -563,11 +557,10 @@
//          "targetAccount": "book_store",
//          "time": "2021-03-22T10:29:54.658Z",
//        },
//        Object {
//          "amount": -7000,
//    -     "category": "eating_out",
//          "currency": "EUR",
//          "id": "722362d1-fbbb-418b-9d15-c00411c2834e",
//          "sourceAccount": "my_account",
//          "targetAccount": "restaurant",
//          "time": "2021-04-11T20:27:01.007Z",
//    @@ -580,38 +573,34 @@
//          "targetAccount": "coffee_shop",
//          "time": "2021-02-06T12:52:40.479Z",
//        },
//        Object {
//          "amount": -9300,
//    -     "category": "entertainment",
//          "currency": "EUR",
//          "id": "f3a6d626-3fde-464b-a82b-fd697f5728a7",
//          "sourceAccount": "my_account",
//          "targetAccount": "book_store",
//          "time": "2021-03-22T14:47:34.264Z",
//        },
//        Object {
//          "amount": -8500,
//    -     "category": "eating_out",
//          "currency": "EUR",
//          "id": "451cc1bf-5096-4dac-817c-4a9199983469",
//          "sourceAccount": "my_account",
//          "targetAccount": "restaurant",
//          "time": "2021-02-09T22:15:27.803Z",
//        },
//        Object {
//          "amount": -10000,
//    -     "category": "eating_out",
//          "currency": "EUR",
//          "id": "d6e3d0ab-c706-45ab-839e-95cca75a615c",
//          "sourceAccount": "my_account",
//          "targetAccount": "coffee_shop",
//          "time": "2021-02-04T10:34:28.363Z",
//        },
//        Object {
//          "amount": -8900,
//    -     "category": "eating_out",
//          "currency": "EUR",
//          "id": "c42899aa-01b1-4db3-8b3f-6dc506d525f3",
//          "sourceAccount": "my_account",
//          "targetAccount": "coffee_shop",
//          "time": "2021-02-20T01:31:36.567Z",
//    @@ -640,11 +629,10 @@
//          "targetAccount": "book_store",
//          "time": "2021-02-19T16:27:44.916Z",
//        },
//        Object {
//          "amount": -1700,
//    -     "category": "groceries",
//          "currency": "EUR",
//          "id": "90149102-8fa5-4e26-b693-c2eca0b8077f",
//          "sourceAccount": "my_account",
//          "targetAccount": "supermarket",
//          "time": "2021-02-01T09:15:26.351Z",
//    @@ -692,20 +680,18 @@
//          "targetAccount": "bowling_alley",
//          "time": "2021-03-08T21:25:56.505Z",
//        },
//        Object {
//          "amount": -8300,
//    -     "category": "eating_out",
//          "currency": "EUR",
//          "id": "457a051c-462c-412f-bc99-fe4272601cab",
//          "sourceAccount": "my_account",
//          "targetAccount": "coffee_shop",
//          "time": "2021-02-17T00:25:57.763Z",
//        },
//        Object {
//          "amount": -3400,
//    -     "category": "eating_out",
//          "currency": "EUR",
//          "id": "e20abe7a-5ab3-4b6d-9c27-8685ba2908ba",
//          "sourceAccount": "my_account",
//          "targetAccount": "coffee_shop",
//          "time": "2021-04-09T22:44:51.727Z",
//
//          ---- crazy version
//          const absoluteDifference = (firstAmount, secondAmount) =>   Math.abs(
//  Math.abs(parseFloat(firstAmount)) - Math.abs(parseFloat(secondAmount))
//);
//const validateTransaction = (curAmount, amount, category) =>
//category === undefined ||
//      Math.abs(amount) > Math.abs(curAmount);
//
//const mapStoreViaCategory = (item, map) => {
//  if (map.has(item.targetAccount)) {
//    let { amount, category } = item;
//    let currentAmount = map.get(item.targetAccount).amount;
//    if (validateTransaction(currentAmount, amount, category)) return;
//        let newVal = map.get(item.targetAccount);
//    newVal.category = [...newVal.category, category];
//    map.set(item.targetAccount, newVal);
//
//  } else {
//        if (item.hasOwnProperty("category")) {
//      let new_map = new Map();
//      map.set(item.targetAccount, { ...item, category: [item.category] });
//    }
//  }
//
//  };
//
//
//const highestFrequencyCategory = (arr) => {
//  const counterMap = new Map();
//  for (const item of arr) {
//    counterMap.set(item, counterMap.get(item) + 1 || 1);
//  }
//  let temp = 0;
//  let category;
//  for (const [key, value] of counterMap.entries()) {
//    if (value > temp) {
//      category = key;
//      temp = value;
//    }
//  }
//  return category;
//};
//
//
//const appendCategoryForTransaction = (item, map) => {
//  if (!item.hasOwnProperty("category") && item.targetAccount !== undefined) {
//    const account = map.get(item.targetAccount);
//    if (account !== null && account !== undefined) {
//      const { category, amount } = account;
//      const mostFrequentCategory = highestFrequencyCategory(category);
//
//      if (
//        category !== undefined &&
//        absoluteDifference(item.amount, amount) < 1000
//      ) {
//                item = { ...item, category: mostFrequentCategory };
//
//      }
//    }
//  }
//  return item;
//};
//
//
//const categorizeSimilarTransactions = (transactions) => {
//  // add your code here
//  if (transactions.length === 0) return [];
//  const store = new Map();
//  transactions.forEach((item) => mapStoreViaCategory(item, store));
//    if (store.size === 0){
//    return transactions;
//  }
//  const updatedTransactions = transactions.map((item) =>
//                                               appendCategoryForTransaction(item, store)
//                                              );
//  return updatedTransactions;
//};
//
//module.exports = categorizeSimilarTransactions
