const longestCommonPrefix = (strs: Array<string>): string => {
  if (strs.length === 0) {
    return "";
  }
  // sort to set an initial value
  let sortedArr = strs.sort((a, b) => a.length - b.length);
  let shortedString = sortedArr[0];
  // use the first element to check if every item starts with the same, if not, remove last item from the array and recheck
  while (!strs.every((item) => item.startsWith(shortedString))) {
    if (shortedString.length === 0) {
      break;
    }
    shortedString = shortedString.slice(0, -1);
  }
  return shortedString;
};
export const main = () => {
  console.log(longestCommonPrefix(["cat", "caterpillar", "call"]));
};
