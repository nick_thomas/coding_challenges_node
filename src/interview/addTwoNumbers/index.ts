class ListNode {
  value: number;
  next: ListNode | null;
  constructor(val: number, valu: ListNode | null) {
    this.value = val;
    this.next = valu;
  }
}

const addTwoNumbers = (
  first: ListNode | null,
  second: ListNode | null
): ListNode => {
  let res: ListNode | null = null;
  let prev: ListNode | null = null;
  let temp: ListNode | null = null;
  let [sum, carry] = [0, 0];
  while (first !== null && second !== null) {
    sum =
      carry +
      (first !== null ? first.value : 0) +
      (second !== null ? second.value : 0);
    carry = sum > 10 ? 1 : 0;
    sum = sum % 10;
    temp = new ListNode(sum, null);
    if (res === null) {
      res = temp;
    } else {
      prev!.next = temp;
    }
    prev = temp;
    if (first !== null) {
      first = first.next;
    }
    if (second !== null) {
      second = second.next;
    }
    if (carry > 0) {
      temp.next = new ListNode(carry, null);
    }
  }
  return res!;
};
export const main = () => {
  const value1 = new ListNode(4, new ListNode(4, new ListNode(7, null)));
  const value2 = new ListNode(2, new ListNode(9, new ListNode(1, null)));
  console.log(addTwoNumbers(value1, value2));
};
