class TreeNode {
  val: number;
  left: TreeNode | null;
  right: TreeNode | null;
  constructor(val?: number, left?: TreeNode | null, right?: TreeNode | null) {
    this.val = val === undefined ? 0 : val;
    this.left = left === undefined ? null : left;
    this.right = right === undefined ? null : right;
  }
}
function maxDepthOfBinaryArray(root: TreeNode | null): number {
  if (root === null) {
    return 0;
  } else if (root.left === null && root.right === null) {
    return 1;
  }
  let rightSideNode = maxDepthOfBinaryArray(root.right) + 1;
  let leftSideNode = maxDepthOfBinaryArray(root.left) + 1;
  return Math.max(leftSideNode, rightSideNode);
}
export const main = () => {
  const root = new TreeNode(
    19,
    new TreeNode(4, null, null),
    new TreeNode(
      7,
      new TreeNode(11, new TreeNode(5, null, null), null),
      new TreeNode(8, null, null)
    )
  );
  console.log(root);
  console.log(maxDepthOfBinaryArray(root));
};
