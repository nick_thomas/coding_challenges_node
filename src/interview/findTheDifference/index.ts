const findTheDifference = (s: string, t: string): string => {
  let output = "";
  let newMap: Map<string, number> = new Map();
  [...s].forEach((item) => {
    newMap.set(item, newMap.get(item) || 1);
  });
  [...t].forEach((item) => {
    if (!newMap.has(item)) {
      output = item;
    }
  });
  return output;
};
export const main = () => {
  console.log(findTheDifference("", "z"));
};
