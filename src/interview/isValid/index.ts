function isValidParentheses(str: string): boolean {
  const stack: Array<string> = [];
  for (const item of str) {
    if (item === ")" && stack[0] === "(") {
      stack.shift();
      continue;
    }
    if (item === "]" && stack[0] === "[") {
      stack.shift();
      continue;
    }
    if (item === "}" && stack[0] === "{") {
      stack.shift();
      continue;
    }
    stack.unshift(item);
  }
  return stack.length === 0 ? true : false;
}
export const main = () => {
  console.log(isValidParentheses("()[]{}"));
  console.log(isValidParentheses("({)}"));
  console.log(isValidParentheses("{[]}"));
};
