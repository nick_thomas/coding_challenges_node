import permutationsIndex from "./Backtracking/GeneralPermuations.js";
import knighttour from "./Backtracking/knighttour.js";
import { nQueenPractice } from "./Backtracking/nqueen_practice.js";
import { mainSetBit } from "./bit-manipulation/setbit.js";
const main = () => {
  //  permutationsIndex();
  //knighttour();
  nQueenPractice();
};
export default main;
