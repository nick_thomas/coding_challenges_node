// to set a bit at particular index
// << is to shift index to the postion where we want to change in the number << creates a binary number with leading zeros 1 << 2 -> 100
// | operator is then used to change at that particular position
const setBit = (num: number, index: number): number => {
  console.log((1 << 3).toString(2));
  let number = num | (1 << index);
  return number;
};

export const mainSetBit = () => {
  console.log(setBit(10, 2));
};
