type KnightMoves = [number, number];
type Board = Array<Array<number>>;
class KnightTourPractice {
  public size: number;
  public board: Board;
  constructor(size: number) {
    this.size = size;
    this.board = new Array(size).fill(0).map(() => new Array(size).fill(0));
  }
  getMoves([x, y]: KnightMoves): Board {
    const moves: Board = [
      [x + 2, y - 1],
      [x + 2, y + 1],
      [x - 2, y - 1],
      [x - 2, y + 1],
      [x + 1, y - 2],
      [x + 1, y + 2],
      [x - 1, y - 2],
      [x - 1, y + 2],
    ];
    return moves.filter(
      ([i, j]) => j >= 0 && j < this.size && i >= 0 && i < this.size
    );
  }

  complete(): boolean {
    return !this.board.map((row) => row.includes(0)).includes(true);
  }
  solve() {
    for (let i = 0; i < this.size; i++) {
      for (let j = 0; j < this.size; j++) {
        if (this.solveHelper([i, j], 0)) {
          return true;
        }
      }
    }
    return false;
  }
  solveHelper([i, j]: KnightMoves, curr: number): boolean {
    if (this.complete()) {
      return true;
    }
    for (const [y, x] of this.getMoves([i, j])) {
      if (this.board[y][x] === 0) {
        this.board[y][x] = curr + 1;
        if (this.solveHelper([y, x], curr + 1)) return true;
        this.board[y][x] = 0;
      }
    }
    return false;
  }
}

export default KnightTourPractice;
