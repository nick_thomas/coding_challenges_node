// https://www.britannica.com/science/permutation
//
type Response = Array<Array<number>>;

const swap = (arr: Array<number>, i: number, j: number): Array<number> => {
  let newArr = [...arr];
  [newArr[i], newArr[j]] = [newArr[j], newArr[i]];
  return newArr;
};

const permute = (arr: Array<number>): Response => {
  let P: Response = [];
  const permuteRun = (
    arr: Array<number>,
    low: number,
    high: number
  ): Response => {
    if (low === high) {
      P.push([...arr]);
      return P;
    }
    for (let i = low; i <= high; i++) {
      arr = swap(arr, low, i);
      permuteRun(arr, low + 1, high);
    }
    return P;
  };
  return permuteRun(arr, 0, arr.length - 1);
};

const permutationsIndex = () => {
  console.log(permute([1, 2, 3]));
};

export default permutationsIndex;
