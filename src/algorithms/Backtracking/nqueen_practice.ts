class NQueen {
  public board: Array<Array<string>>;
  public size: number;
  public solutionCount = 0;
  constructor(size: number) {
    this.board = new Array(size).fill(".").map(() => new Array(size).fill("."));
    this.size = size;
    this.solutionCount = 0;
  }
  isValid([row, col]: [number, number]): boolean {
    for (let i = 0; i < col; i++) {
      if (this.board[row][i] === "Q") return false;
    }
    {
      let i = row;
      let j = col;
      for (; i >= 0 && j >= 0; i--, j--) {
        if (this.board[i][j] === "Q") return false;
      }
    }

    {
      let i = row;
      let j = col;
      for (; j >= 0 && i < this.size; i++, j--) {
        if (this.board[i][j] === "Q") return false;
      }
    }

    return true;
  }

  placeQueen(row: number, col: number) {
    this.board[row][col] = "Q";
  }
  removeQueen(row: number, col: number) {
    this.board[row][col] = ".";
  }
  solve(col: number = 0): boolean {
    if (col >= this.size) {
      this.solutionCount++;
      return true;
    }
    for (let i = 0; i < this.size; i++) {
      if (this.isValid([i, col])) {
        this.placeQueen(i, col);
        this.solve(col + 1);
        this.removeQueen(i, col);
      }
    }
    return false;
  }
}
export const nQueenPractice = () => {
  const queen = new NQueen(4);
  queen.solve();
  console.log(queen.board);
};
