// https://betterexplained.com/articles/easy-permutations-and-combinations/
// Combinations are where order doesn't matter e.x groups
// Permutations are for lists, where order matters -> all possible way of doing something
class Combinations {
  public n: number;
  public k: number;
  public current: Array<number>;
  public combinations: Array<Array<number>>;

  constructor(n: number, k: number) {
    this.n = n;
    this.k = k;
    this.current = [];
    this.combinations = [];
  }
  public findCombinations(high = this.n, total = this.k, low = 1) {
    if (total === 0) {
      this.combinations.push([...this.current]);
      return this.combinations;
    }
    for (let i = low; i <= high; i += 1) {
      this.current.push(i);
      this.findCombinations(high, total - 1, i + 1);
      this.current.pop();
    }
    return this.combinations;
  }
}
const mainCombination = () => {
  let combination = new Combinations(5, 2);
  console.log(combination.findCombinations().length);
};
export default mainCombination;
