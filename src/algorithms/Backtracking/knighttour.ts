// Wikipedia: https://en.wikipedia.org/wiki/Knight%27s_tour

import KnightTourPractice from "./knighttour_practice.js";

class KnightTour {
  public size: number;
  public board: Array<Array<number>>;
  constructor(size: number) {
    this.size = size;
    this.board = new Array(size).fill(0).map(() => new Array(size).fill(0));
  }
  getMoves([i, j]: [number, number]): Array<Array<number>> {
    const moves = [
      [i + 2, j - 1],
      [i + 2, j + 1],
      [i - 2, j - 1],
      [i - 2, j + 1],
      [i + 1, j - 2],
      [i + 1, j + 2],
      [i - 1, j - 2],
      [i - 1, j + 2],
    ];
    return moves.filter(
      ([y, x]) => y >= 0 && y < this.size && x >= 0 && x < this.size
    );
  }

  isComplete(): boolean {
    return !this.board.map((row) => row.includes(0)).includes(true);
  }

  solve(): boolean {
    for (let i = 0; i < this.size; i++) {
      for (let j = 0; j < this.size; j++) {
        if (this.solveHelper([i, j], 0)) {
          return true;
        }
      }
    }
    return false;
  }
  solveHelper([i, j]: [number, number], curr: number): boolean {
    if (this.isComplete()) return true;

    for (const [y, x] of this.getMoves([i, j])) {
      if (this.board[y][x] == 0) {
        this.board[y][x] = curr + 1;
        if (this.solveHelper([y, x], curr + 1)) return true;
        this.board[y][x] = 0;
      }
    }
    return false;
  }
}

const knightTourMain = () => {
  const knight = new KnightTour(5);
  knight.solve();
  console.log(knight.board);
  const newKnight = new KnightTourPractice(5);
  newKnight.solve();
  console.log(newKnight.board);
};

export default knightTourMain;
