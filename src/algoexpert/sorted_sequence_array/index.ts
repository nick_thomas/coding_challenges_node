function sortedSquaredArray(array: number[]) {
  // Write your code here.
 const result= array.map(number=> Math.pow(number, 2)).sort((a,b)=> a.toString().localeCompare(b.toString(),'en-US', {numeric: true}
 ))
 return result
}
export function main(){
	console.log(sortedSquaredArray([0,0,1,1,1,2,2,3,13,19,20,50]));
}
