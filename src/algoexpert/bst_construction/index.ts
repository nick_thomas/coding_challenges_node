// Do not edit the class below except for
// the insert, contains, and remove methods.
// Feel free to add new properties and methods
// to the clas
class BST {
	value: number;
	left: BST | null;
	right: BST | null;

	constructor(value: number) {
		this.value = value;
		this.left = null;
		this.right = null;
	}

	insert(value: number): BST {
		// Write your code here.
		// Do not edit the return statement of this method.
		if (this.value < value) {
			if (this.right === null){
				this.right = new BST(value);
				return this;
			}
			return this.right.insert(value);
		} else if (this.value > value) {
			if (this.left === null) {
				this.left = new BST(value);
				return this;
			}
			return this.left.insert(value);
		}
		return this;
	}

	contains(value: number) {
		// Write your code here.
		if (this.value === value){
			return true;
		} else if (this.value > value) {
			if (this.left !== null){ 
				let result = this.left.contains(value) as boolean;
				return result;
			}else {
				return false;
			};
		} else if (this.value < value){
			if (this.right !== null){
				let result = this.right.contains(value) as boolean;
				return result
			} else {
				return false;
			}
		} 

	}

	remove(value: number, parent?: BST | null): BST {
		// Write your code here.
		// Do not edit the return statement of this method.
		if(value < this.value){
			if(this.left !== null){
				this.left.remove(value,this);
			}
		} else if(value > this.value){
			if (this.right !== null){
				this.right.remove(value,this);
			}
		} else {
			if(this.left !== null && this.right !== null){
				this.value = this.right.getMinValue();
				this.right.remove(this.value,this);
			} else if (parent === null){
				if(this.left !==null){
					this.value = this.left.value;
					this.right = this.left.right;
					this.left = this.left.left;
				} else if (this.right !== null){
					this.value = this.right.value;
					this.left = this.right.left;
					this.right = this.right.right;
				} else {

				}
			} else if (parent !== undefined && parent.left === this){
				parent.left = this.left ? this.left : this.right;
			} else if (parent !== undefined && parent.right === this){
				parent.right = this.left ? this.left : this.right;
			}
		}
		return this;
	}

	getMinValue():number {
		if (this.left === null){
			return this.value;
		}else {
			return this.left.getMinValue();
		}
	}
}

export const main = () => {
	const root = new BST(10);
	root.left = new BST(5);
	root.left.left = new BST(2);
	root.left.left.left = new BST(1);
	root.left.right = new BST(5);
	root.right = new BST(15);
	root.right.left = new BST(13);
	root.right.left.right = new BST(14);
	root.right.right = new BST(22);
	root.insert(12);
	root.remove(10);
	console.log(root.contains(15));
}
