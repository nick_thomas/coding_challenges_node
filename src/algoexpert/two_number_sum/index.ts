function twoNumberSum(array: number[], targetSum: number) {
	// Write your code here.
	let output: Array<number> = [];
	for (const [index,item] of array.entries()){
		for (const secondItem of array.slice(index+1,array.length).values()){
			if (item + secondItem === targetSum){
				output = [item,secondItem];
				break;
			}
		}
	}
	return output
}

export function main() {
	let value = twoNumberSum([3,5,-4,8,11,1,-1,6],10);
	console.log(value)
}
