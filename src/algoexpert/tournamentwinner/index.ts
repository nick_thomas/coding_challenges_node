function tournamentWinner(competitions: string[][], results: number[]) {
	// Write your code here.
	let scoreBoard: Map<string,number> = new Map();
	for (const [index, language] of Object.entries(competitions)){
		const [first,second] = language;
		let score = results[parseInt(index)];
		let winningTeam = score ? first : second;
		if (scoreBoard.has(winningTeam)){
			let currentValue = scoreBoard.get(winningTeam)!;
			scoreBoard.set(winningTeam,currentValue+3);
		} else {
			scoreBoard.set(winningTeam, 3)
		}
	}
	const [output,] = [...scoreBoard.entries()].reduce((acc,curr)=> curr[1] > acc[1] ? curr: acc);
	return output;
}

export function main(){
	let competitions = [
		["HTML", "C#"],
		["C#", "Python"],
		["Python", "HTML"]
	];
	let results = [0,0,1]
	console.assert(tournamentWinner(competitions, results) === "Python","fail")

}
