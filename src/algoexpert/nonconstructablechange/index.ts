export function nonConstructibleChange(coins: number[]) {
	// Write your code here.
	let sortedCoins = [...coins].sort((a,b)=> a.toString().localeCompare(b.toString(),undefined,{numeric: true}));
	let output = 0;
	for (const coin of sortedCoins){
		if (coin > output+1) return output+1;
		output+=coin;
	}
	return output+1;
}

export function main(){
	console.assert(nonConstructibleChange([6,4,5,1,1,8,9]) === 3, "Output fail");
}
