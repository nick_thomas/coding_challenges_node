export const boolToWord = (bool: boolean): string => {
  interface Response {
    true: string;
    false: string;
  }
  const ResponseObject: Response = {
    true: "Yes",
    false: "No",
  };
  type ResponseString = keyof Response;
  const value: ResponseString = bool.toString() as ResponseString;
  return ResponseObject[value];
};
export const main = () => {
  console.log(boolToWord(false));
};
