function parse(data: string): number[] {
    let commands = data.split("");
    let seed = 0;
    let output: Array<number> = [];
    for (let item of commands) {
        switch (item) {
            case 'i':
                seed += 1;
                break;
            case 'd':
                seed -= 1;
                break;
            case 's':
                seed = Math.pow(seed, 2);
                break;
            case 'o':
                output.push(seed);
                break;
            default:
                continue;
        }

    }
    return output
}

export const main = () => {
    console.log(parse("iiisdoso"))
}