const likes = (a: string[]): string => {
  switch (a.length) {
    case 0:
      return "no one likes this";
    case 1: {
      const [name] = a;
      return `${name} likes this`;
    }
    case 2: {
      const [first, second] = a;
      return `${first} and ${second} like this`;
    }
    case 3: {
      const name = a.reduce((acc, cur, index) => {
        if (index === a.length - 1) {
          acc += `and ${cur}`;
          return acc;
        }
        if (index === 0) {
          acc += `${cur},`;
          return acc;
        }
        acc += ` ${cur} `;
        return acc;
      }, "");
      return `${name.trim()} like this`;
    }
    default:
      const [first, second, ...rest] = a;
      return `${first}, ${second} and ${rest.length} others like this`;
  }
};
export const main = () => {
  console.assert(likes([]) === "no one likes this");
  console.assert(likes(["Peter"]) === "Peter likes this");
  console.assert(likes(["Jacob", "Alex"]) === "Jacob and Alex like this");
  console.assert(
    likes(["Max", "John", "Mark"]) === "Max, John and Mark like this"
  );
  console.assert(
    likes(["Alex", "Jacob", "Mark", "Max"]),
    "Alex, Jacob and 2 others like this"
  );
};
