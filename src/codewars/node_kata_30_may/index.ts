function solution(str: string, ending: string): boolean {
	if (ending.length == 0) {
		return true
	}

	if (str.slice(-ending.length) == ending) {
		return true
	}

	return false; 
}

export const main = () => {
	console.log(solution('abcde', 'cde'));
	console.log(solution('abcde', 'abc'));
	console.log(solution('abcde', ''));

}
