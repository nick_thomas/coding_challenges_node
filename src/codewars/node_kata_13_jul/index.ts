function removeChar(str: string): string {
	// You got this!
	let stringContainer: string[] = Array.from(str);
	stringContainer.shift();
	stringContainer.pop();
	return 	stringContainer.join("");
}

export const main = () => {
	console.log(removeChar("place"))
}
