export function findSmallestInt(args: number[]): number {
	let args_sorted= args.sort((a,b)=> Number(a) > Number(b)? 1:-1);
	return args_sorted[0];
}

export const main = ()=> { 
	console.log(findSmallestInt([78,56,232,12,0]))
}
