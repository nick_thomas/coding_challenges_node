// 1736
// You are given a string time in the form of hh:mm, where some of the digits in the string are hidden (represented by ?).
// The valid times are those inclusively between 00:00 and 23:59.
// Return the latest valid time you can get from time by replacing the hidden digits.

function maximumTime(time: string): string {
  let [first, second] = time.split(":");
  let temparr = first.split("")
  {
    let index = temparr.findIndex((element) => element === "?");
    do {
      if (index > -1) {
        let [head, tail] = temparr;
        switch (index) {
          case 0: {
            if ([0, 1, 2, 3].includes(Number(tail))) {
              temparr[0] = "2"
            } else if (tail != "?") {
              temparr[0] = "1"
            } else {
              temparr[0] = "2";
            }
            break;
          }
          case 1: {
            if ('2' === head) {
              temparr[1] = "3"
            } else {
              temparr[1] = "9"
            }
            break;
          }
        }
      }
      index = temparr.findIndex((element) => element === "?");
    } while (index != -1)
  }
  let tempminute = second.split("");
  {
    let index = tempminute.findIndex((element) => element === "?");
    do {
      if (index > -1) {
        switch (index) {
          case 0: {
            tempminute[index] = "5"
            break;
          }
          case 1: {
            tempminute[index] = "9"
            break;
          }
        }
      }
      index = tempminute.findIndex((element) => element === "?");
    } while (index != -1)
  }
  return [...temparr, [":"], ...tempminute].join("");
};
export const main = () => {
  console.log(maximumTime("?4:03"));
};
