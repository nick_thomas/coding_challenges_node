export class Kata {
  static vowels = ["a", "e", "i", "o", "u"];
  static disemvowel(str: string): string {
    let split_strings = str.split(" ");
    let response = split_strings.map((item) => {
      let split_each_word = item.split("");
      let output = split_each_word.filter(
        (each_item) =>
          !this.vowels.some((element) => element === each_item.toLowerCase())
      );
      return output.join("");
    });
    return response.join(" ");
  }
}
export const main = () => {
  let output = Kata.disemvowel("This website is for losers LOL!");
  console.log(output);
};
