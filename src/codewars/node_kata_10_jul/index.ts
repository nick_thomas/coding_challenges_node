export function orderWeight(strng: string): string {
	// Your code
	const input = strng.split(" ");
	input.sort(function(a,b) {
		return sort_by_element(a,b);
	})
	return	input.join(" ");
}
function sort_by_element(a:string, b:string): number {
	const first: number = a.split("").reduce((acc,cum)=>{
		return acc + parseInt(cum);
	},0);
	const second: number = b.split("").reduce((acc,cum)=>{
		return acc + parseInt(cum);
	},0);
	if (first < second){
		return -1;
	}
	if (first > second){
		return 1;
	}
	return a.localeCompare(b);
}
export const main = () => {
	console.log(orderWeight("2000 10003 1234000 44444444 9999 11 11 22 123"));
}
