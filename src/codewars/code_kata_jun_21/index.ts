function *fibonacciSequence(): Iterator<number> {
	// To be implemented: Proper implementation.
	let current = 0;
	let next = 1;
	while (true){ 
		if (current !==0) {
			yield current;
		}
		[current,next] = [next,next+current];
	}
}
function simpleArraySum(ar: number[]): number {
	// Write your code here
return ar.reduce((acc,curr)=> acc+curr,0);

}
export const main= () => {
	console.log(simpleArraySum([1,2, 3, 4, 10, 11]))
}

class Foo {
bar(){
	var x = 3;
}

}
