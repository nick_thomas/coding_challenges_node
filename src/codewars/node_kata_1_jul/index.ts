function comp(a1: number[] | null, a2: number[] | null): boolean {
	// your code
	if (a1 !== null && a2 !== null && a1.length === a2.length){
		for (const item of a1) {
			let squaredNumber = item ** 2;
			if (!a2.includes(squaredNumber)) {
				return false;
			} 
		}
		return true;
	}
	else {
		return false;
	}
}
export const  main = ( ) => {
	var a1 = [121, 144, 19, 161, 19, 144, 19, 11];
	var a2 = [11*21, 121*121, 144*144, 19*19, 161*161, 19*19, 144*144, 19*19];
	console.log(comp(a1, a2));
}
