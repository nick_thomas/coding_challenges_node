function sumOfIntervals(intervals: [number, number][]): number {
    // your code here
    let sorted_interval = intervals.sort((a, b) => a[0] > b[0] ? 1 : -1);
    let current_max = 0;
    let previous_entry: [number, number] = [0, 0];
    let output = 0;
    for (let i = 0; i < sorted_interval.length; i++) {
        if (current_max === 0) {
            current_max = sorted_interval[i][1];
            previous_entry = sorted_interval[i];
            output = sorted_interval[i][1] - sorted_interval[i][0];
            continue;
        }
        if (arrEq(previous_entry, sorted_interval[i])) {
            continue;
        }
        if (current_max)
            if (current_max > sorted_interval[i][0] && current_max < sorted_interval[i][1]) {
                output += sorted_interval[i][1] - current_max;
                continue;
            }
        output += sorted_interval[i][1] - sorted_interval[i][0];
    }
    return output;
}

function arrEq(arr1: [number, number], arr2: [number, number]): boolean {
    let arr_length;
    if ((arr_length = arr1.length) != arr2.length) return false;
    for (let i = 0; i < arr_length; i++) if (arr1[i] != arr2[i]) return false;
    return true;
}
export const main = () => {
    console.log(sumOfIntervals([[1, 5], [1, 5]]));
}