const compareTriplets = (a: Array<number>, b: Array<number>): Array<number> => {
    let leftCount = 0;
    let rightCount = 0;
    for (let i = 0; i < a.length; i += 1) {
        if (a[i] > b[i]) {
            leftCount += 1;
        } else if (a[i] < b[i]) {
            rightCount += 1;
        }
    }
    return [leftCount, rightCount]
}
export const main = () => {
    console.log(compareTriplets([17, 28, 30], [99, 16, 8]))
}
